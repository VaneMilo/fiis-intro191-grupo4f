//============================================================================
// Name        : Reservaci�n.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
using namespace std;
void inicio();

void pantallausuario();
void registroUsuario();
void loginUsuario();
void usuarioLogeado(string user);

void pantallarestaurant();
void registroRestaurant();
void loginRestaurant();
void restaurantLogeado(string user);

void mostrarRes();
void reserva(string user);

//Todo para usuarios
struct usuario
{
    string user;
    string contra;
};

struct restaurant
{
    string user;
    string contra;
};

void mostrarRes(){
	char cadena[200];
  	ifstream fe("datos.txt");
  	while (!fe.eof()) {
    fe >> cadena;
    cout << cadena << endl;
  	}
  	fe.close();
}

void reserva(string user){
	mostrarRes();
	string res="";
	string dia;
	string rest;
	string hora;
	string mesa;
	cout<<"Ingrese el restaurant en el que desea reservar:";
	getline(cin,rest);
	res=res+rest+',';
	cout<<"Ingrese el dia que desea reservar:";
	getline(cin,dia);
	res=res+dia+',';
	cout<<"Ingrese la hora en la que desea reservar:";
	getline(cin,hora);
	res=res+hora+',';
	cout<<"Ingrese la cantidad de personas para la reserva:";
	getline(cin,mesa);
	res=res+mesa;
	ofstream file;
  	file.open("usuarios.txt",ios::app);
 	file << res;
  	file.close();

}

void usuarioLogeado(string user){
	cout<<"\n\n\n\n    	Bienvenido Usuario!"<<endl<<"   	Elija la opcion a realizar"<<endl;
	cout<<"   	1.- Realizar reserva"<<endl;
	cout<<"   	2.- Cerrar sesi�n"<<endl;
	cout<<"_____________________________________________________________"<<endl;
	cout << "	Seleccionar la opcion: ";
	int op;
	cin>>op;
	switch(op){
    case 1:
    	reserva(user);
    break;
    case 2:
    	loginUsuario();
    break;
    default:
		cout << "Usted ha ingresado una opci�n incorrecta";
		usuarioLogeado(user);
	}
}

void loginUsuario(){
	string user1="";
	cout<<"\n\n\n\nIngrese el nombre de usuario:";
	getline(cin,user1);
	string contra1="";
	cout<<"      Ingrese la contrasena:";
	getline(cin,contra1);
	int log=0;
	//validar ingreso
	usuarioLogeado(user1);
}

void registroUsuario(){
	string user="";
	cout<<"\n\n\n\nIngrese el nombre de usuario:";
	getline(cin,user);
	string contra="";
	cout<<"Ingrese la contrasena a usar:";
	getline(cin,contra);
	ofstream file;
  	file.open("usuarios.txt",ios::app);
 	file << user+','+contra;
  	file.close();
}

void pantallausuario(){
	cout<<"\n\n\n\n    	Bienvenido Usuario!"<<endl<<"   	Elija la opcion a realizar"<<endl;
	cout<<"   	1.- Registro"<<endl;
	cout<<"   	2.- Login"<<endl;
	cout<<"_____________________________________________________________"<<endl;
	cout << "	0. Regresar a la pantalla anterior\n\n";
	cout << "	Seleccionar la opcion: ";
	int op;
	cin>>op;
	switch(op){
    case 0:
		inicio();
    break;
    case 1:
		registroUsuario();
    break;
    case 2:
		loginUsuario();
    break;
    default:
		cout << "Usted ha ingresado una opci�n incorrecta";
		pantallausuario();
	}
}

//Todo para restaurantes

void restaurantLogeado(string user){
	cout<<"\n\n\n\n    	Bienvenido "<< user<<"!"<<endl<<"   	Elija la opcion a realizar"<<endl;
	cout<<"   	1.- Llenar datos"<<endl;
	cout<<"   	2.- Ver reservas"<<endl;
	cout<<"_____________________________________________________________"<<endl;
	cout << "	0. Regresar a la pantalla inicio\n\n";
	cout << "	Seleccionar la opcion: ";
	int op;
	cin>>op;
	switch(op){
    case 0:
		inicio();
    break;
    case 1:
    	string datos=user;
    	string ite="";
		cout<<"Ingrese la direccion:  ";
    	getline(cin,ite);
    	datos=datos+','+ite;
    	cout<<"Ingrese hora de apertura:  ";
    	getline(cin,ite);
    	datos=datos+','+ite;
    	cout<<"Ingrese hora de cierre:  ";
    	getline(cin,ite);
    	datos=datos+','+ite;
    	string plato="";
		string menu="";
		int num;
		cout<<"Ingrese la cantidad de platos:";
		cin>>num;
		//la cantidad de platos siempre ponle 5
		for(int i=0;i<=5;i++)
		{
    		cout<<"Ingrese el plato "<< i+1<<" :";
    		getline(cin,plato);
			menu=menu+','+plato;
		}
		datos=datos+','+menu;
		ofstream file;
  		file.open("datos.txt",ios::app);
 		file << datos;
  		file.close();
  		cout<<"Datos subidos exitosamente!"<<endl;
    break;
    case 2:
    	char cadena[200];
  		ifstream fe("reservas.txt");
  		while (!fe.eof()) {
    	fe >> cadena;
    	cout << cadena << endl;
  		}
  		fe.close();
    break;
    default:
		cout << "Usted ha ingresado una opci�n incorrecta";
		restaurantLogeado(user);
	}
}

void loginRestaurant(){
	string user1="";
	cout<<"\n\n\n\nIngrese el nombre de Restaurant:";
	getline(cin,user1);
	string contra1="";
	cout<<"      Ingrese la contrasena:";
	getline(cin,contra1);
	int log=0;
	//validar ingreso
	restaurantLogeado(user1);
}

void registroRestaurant(){
	string user="";
	cout<<"\n\n\n\nIngrese el nombre de Restaurant:";
	getline(cin,user);
	string contra="";
	cout<<"Ingrese la contrasena a usar:";
	getline(cin,contra);
	ofstream file;
  	file.open("restaurantcuentas.txt",ios::app);
 	file << user+','+contra;
  	file.close();
}

void pantallarestaurant(){
	cout<<"\n\n\n\n    	Bienvenido Restaurant!"<<endl<<"   	Elija la opcion a realizar"<<endl;
	cout<<"   	1.- Registro"<<endl;
	cout<<"   	2.- Login"<<endl;
	cout<<"_____________________________________________________________"<<endl;
	cout << "	0. Regresar a la pantalla anterior\n\n";
	cout << "	Seleccionar la opcion: ";
	int op;
	cin>>op;
	switch(op){
    case 0:
		inicio();
    break;
    case 1:
		registroRestaurant();
    break;
    case 2:
		loginRestaurant();
    break;
    default:
		cout << "Usted ha ingresado una opci�n incorrecta";
		pantallarestaurant();
	}
}

//menu inicio
void inicio(){
	cout << "\n\n\n\n            Menu - Sistema Resutarante/Usuario"<<endl;
	cout<<"_____________________________________________________________"<<endl;
	cout<<"   	Bienvenido!"<<endl<<"   	Elija la opcion de ingreso"<<endl;
	cout<<"   	1.- Usuario"<<endl;
	cout<<"   	2.- Restaurante"<<endl;
	cout<<"_____________________________________________________________"<<endl;
	cout << "	0. Salir del Programa\n\n";
	cout << "	Seleccionar la opcion: ";
	int op;
	cin>>op;
	switch(op){
    case 0:
		cout << "Gracias!";
    break;
    case 1:
		pantallausuario();
    break;
    case 2:
		pantallarestaurant();
    break;
    default:
		cout << "Usted ha ingresado una opci�n incorrecta";
		inicio();
	}
}


int main(int argc, char** argv) {
	inicio();
}
